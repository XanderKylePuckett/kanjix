﻿using System.Collections.Generic;
using UnityEngine;
using Xander.NullConversion;
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public Material[] characterBackgrounds = null;
    public GameObject characterDisplayPrefab = null;
    [SerializeField] private Camera mainCamera = null;
    private static Camera theCamera = null;
    public const float CharacterWidth = 15.0f;
    public const int MaxCharacters = 12;
    private void Awake()
    {
        if (null == instance)
            instance = this;
        if (this == instance)
        {
            theCamera = mainCamera;
            CameraWidth = CharacterWidth * MaxCharacters;
        }
        else Destroy(this);
    }
    public static float CameraWidth { get { return 2.0f * theCamera.orthographicSize * theCamera.aspect; } set { theCamera.orthographicSize = 0.5f * value / theCamera.aspect; } }
    private void Start()
    {
        InstantiateSentence(new KanjixStructs.KanjixSentence("私[わたし]の名[な]前[まえ]はザンダーです"));
    }
    public CharacterDisplay[] InstantiateSentence(KanjixStructs.KanjixSentence sentence)
    {
        List<CharacterDisplay> characters = new List<CharacterDisplay>();
        for (int i = 0; i < sentence.Count; ++i)
            characters.Add(Instantiate(characterDisplayPrefab, new Vector3(CharacterWidth * (i + (sentence.Count - 1) * -0.5f), 0.0f, 0.0f), Quaternion.identity).GetComponent<CharacterDisplay>().SetCharacter(sentence.Get(i)));
        return characters.ToArray();
    }
    private void OnMouseDown()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            hit.collider.GetComponentInParent<GameButton>().ConvertNull()?.InvokeButtonPress();
    }
}