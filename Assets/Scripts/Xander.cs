namespace Xander.LineEndings
{
    public static class LineEndings
    {
        public static string CRLF2LF(this string str) => str.Replace("\r\n", "\n");
        public static string LF2CRLF(this string str) => str.CRLF2LF().Replace("\n", "\r\n");
    }
}
namespace Xander.NullConversion
{
    public static class NullConversion
    {
        public static T ConvertNull<T>(this T obj) where T : UnityEngine.Object
        {
            if (null == obj)
                return null;
            return obj;
        }
    }
}