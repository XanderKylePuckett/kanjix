﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using KanjixStructs;
using Xander.LineEndings;
public class KanjixLibraries : MonoBehaviour
{
    private static KanjixLibraries theInstance = null;
    private void Awake()
    {
        if (null == theInstance)
            theInstance = this;
        if (this != theInstance)
            Destroy(this);
    }
    private static string KanjiLibraryPath => Application.persistentDataPath + "/KanjiLibrary.dat";
    private static string SentenceLibraryPath => Application.persistentDataPath + "/SentenceLibrary.dat";
    private static KanjixKanjiList m_kanjiLibrary;
    public static KanjixKanjiList GetKanjiLibrary => m_kanjiLibrary;
    private static KanjixSentenceLibrary m_sentenceLibrary;
    public static KanjixSentenceLibrary GetSentenceLibrary => m_sentenceLibrary;
    private void Start()
    {
        LoadKanjiLibrary();
        LoadSentenceLibrary();
        foreach (KanjixCharacter chr in m_sentenceLibrary.Get(0, 0, 0, 0).characters)
            Debug.Log($"CHAR: '{chr.character}'; LEVEL: {chr.level}; KANJI: {(chr.isKanji ? 'T' : 'F')}; READING: \"{chr.reading ?? ""}\"");
    }
    private static void BuildKanjiLibrary()
    {
        List<KanjixKanji> kanjiList = new List<KanjixKanji>();
        for (int i = 0; i < 6; ++i)
            foreach (string line in Resources.Load<TextAsset>($"TextAssets/year{i + 1}kanji").text.CRLF2LF().Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                kanjiList.Add(new KanjixKanji { kanji = line[0], level = i + 1, meaning = line.Substring(2) });
        FileStream file = File.Create(KanjiLibraryPath);
        new BinaryFormatter().Serialize(file, new KanjixKanjiList { list = SortKanji(kanjiList.ToArray()) });
        file.Close();
    }
    private static void BuildSentenceLibrary()
    {
        List<KanjixImportSentence> sentenceList = new List<KanjixImportSentence>();
        string[] lines = Resources.Load<TextAsset>($"TextAssets/sentences").text.CRLF2LF().Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        KanjixImportSentence importSentence;
        for (int i = 0; i < lines.Length; ++i)
        {
            importSentence = new KanjixImportSentence(lines[i], i);
            if (importSentence.IsValid)
                sentenceList.Add(importSentence);
        }
        KanjixImportSentence[] sortedSentences = sentenceList.ToArray();
        Array.Sort(sortedSentences, new ImportSentenceComparer());
        FileStream file = File.Create(SentenceLibraryPath);
        new BinaryFormatter().Serialize(file, new KanjixSentenceLibrary { yearLevels = SplitByYearLevel(sortedSentences) });
        file.Close();
    }
    private static KanjixSentencePracticeCollection[] SplitByYearLevel(KanjixImportSentence[] sentences)
    {
        KanjixSentencePracticeCollection[] retVal = new KanjixSentencePracticeCollection[(int)YearLevel.NumYearLevels];
        int sentenceIdx = 0;
        List<KanjixImportSentence> tmpSentences;
        for (int i = 0; i < retVal.Length; ++i)
        {
            tmpSentences = new List<KanjixImportSentence>();
            if (sentenceIdx < sentences.Length)
                while (i == (int)sentences[sentenceIdx].year)
                {
                    tmpSentences.Add(sentences[sentenceIdx]);
                    ++sentenceIdx;
                    if (sentenceIdx >= sentences.Length)
                        break;
                }
            if (0 != tmpSentences.Count)
                retVal[i].practices = SplitByPracticeNumber(tmpSentences.ToArray());
            else
                retVal[i].practices = null;
        }
        return retVal;
    }
    private static KanjixSentencePractice[] SplitByPracticeNumber(KanjixImportSentence[] sentences)
    {
        KanjixSentencePractice[] retVal = new KanjixSentencePractice[sentences[sentences.Length - 1].practice];
        int sentenceIdx = 0;
        List<KanjixImportSentence> tmpSentences;
        for (int i = 0; i < retVal.Length; ++i)
        {
            tmpSentences = new List<KanjixImportSentence>();
            while (i == sentences[sentenceIdx].practice - 1)
            {
                tmpSentences.Add(sentences[sentenceIdx]);
                ++sentenceIdx;
                if (sentenceIdx >= sentences.Length)
                    break;
            }
            if (0 != tmpSentences.Count)
                retVal[i].pages = SplitByPageNumber(tmpSentences.ToArray());
            else
                retVal[i].pages = null;
        }
        return retVal;
    }
    private static KanjixSentencePracticePage[] SplitByPageNumber(KanjixImportSentence[] sentences)
    {
        KanjixSentencePracticePage[] retVal = new KanjixSentencePracticePage[sentences[sentences.Length - 1].page];
        int sentenceIdx = 0;
        List<KanjixImportSentence> tmpSentences;
        for (int i = 0; i < retVal.Length; ++i)
        {
            tmpSentences = new List<KanjixImportSentence>();
            while (i == sentences[sentenceIdx].page - 1)
            {
                tmpSentences.Add(sentences[sentenceIdx]);
                ++sentenceIdx;
                if (sentenceIdx >= sentences.Length)
                    break;
            }
            if (0 != tmpSentences.Count)
                retVal[i].sentences = KanjixImportSentencesToKanjixSentences(tmpSentences.ToArray());
            else
                retVal[i].sentences = null;
        }
        return retVal;
    }
    private static KanjixSentence[] KanjixImportSentencesToKanjixSentences(KanjixImportSentence[] sentences)
    {
        Array.Sort(sentences, new ImportSentenceComparer());
        KanjixSentence[] retVal = new KanjixSentence[sentences.Length];
        for (int i = 0; i < retVal.Length; ++i)
            retVal[i] = sentences[i].sentence;
        return retVal;
    }
    public static void RebuildKanjiLibrary()
    {
        BuildKanjiLibrary();
        LoadKanjiLibrary();
    }
    public static void RebuildSentenceLibrary()
    {
        BuildSentenceLibrary();
        LoadSentenceLibrary();
    }
    private static void LoadKanjiLibrary()
    {
        if (!File.Exists(KanjiLibraryPath))
            BuildKanjiLibrary();
        FileStream file = File.Open(KanjiLibraryPath, FileMode.Open);
        try { m_kanjiLibrary = (KanjixKanjiList)(new BinaryFormatter().Deserialize(file)); }
        catch
        {
            file.Close();
            BuildKanjiLibrary();
            file = File.Open(KanjiLibraryPath, FileMode.Open);
            try { m_kanjiLibrary = (KanjixKanjiList)(new BinaryFormatter().Deserialize(file)); }
            catch { m_kanjiLibrary = new KanjixKanjiList() { list = null }; }
        }
        file.Close();
    }
    private static void LoadSentenceLibrary()
    {
        if (!File.Exists(SentenceLibraryPath))
            BuildSentenceLibrary();
        FileStream file = File.Open(SentenceLibraryPath, FileMode.Open);
        try { m_sentenceLibrary = (KanjixSentenceLibrary)(new BinaryFormatter().Deserialize(file)); }
        catch
        {
            file.Close();
            BuildSentenceLibrary();
            file = File.Open(SentenceLibraryPath, FileMode.Open);
            try { m_sentenceLibrary = (KanjixSentenceLibrary)(new BinaryFormatter().Deserialize(file)); }
            catch { m_sentenceLibrary = new KanjixSentenceLibrary() { yearLevels = null }; }
        }
        file.Close();
    }
    public class KanjiComparer : IComparer<KanjixKanji> { public int Compare(KanjixKanji x, KanjixKanji y) => x.kanji - y.kanji; }
    public class ImportSentenceComparer : IComparer<KanjixImportSentence>
    {
        public int Compare(KanjixImportSentence x, KanjixImportSentence y)
        {
            int retVal = 0;
            if (0 != (retVal = (int)x.year - (int)y.year)) return retVal;
            if (0 != (retVal = x.practice - y.practice)) return retVal;
            if (0 != (retVal = x.page - y.page)) return retVal;
            return x.importNumber - y.importNumber;
        }
    }
    private static KanjixKanji[] SortKanji(KanjixKanji[] list)
    {
        Array.Sort(list, new KanjiComparer());
        return list;
    }
    private static int FindKanji(char kanji) => Array.BinarySearch(m_kanjiLibrary.list, new KanjixKanji { kanji = kanji }, new KanjiComparer());
    private static int GetLevel(char kanji)
    {
        int idx = FindKanji(kanji);
        if (idx >= 0)
            return m_kanjiLibrary.list[idx].level;
        return -1;
    }
    public static KanjixSentence AssociateKanjiLevels(KanjixSentence sentence)
    {
        if (null == m_kanjiLibrary.list)
            LoadKanjiLibrary();
        for (int i = 0; i < sentence.characters.Length; ++i)
            if (sentence.characters[i].isKanji)
                sentence.characters[i].level = GetLevel(sentence.characters[i].character);
        return sentence;
    }
}