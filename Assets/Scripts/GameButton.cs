﻿using UnityEngine;
public class GameButton : MonoBehaviour
{
    public delegate void ButtonPressedEvent();
    public event ButtonPressedEvent OnButtonPressed;
    public void InvokeButtonPress() => OnButtonPressed?.Invoke();
    [SerializeField] private TextMesh buttonText = null;
    [SerializeField] private MeshRenderer buttonBackground = null;
    [SerializeField] private BoxCollider buttonCollider = null;
    public string ButtonText
    {
        get { return buttonText.text; }
        set { buttonText.text = value; }
    }
    public Vector2 ButtonSize
    {
        get { return new Vector2(buttonBackground.transform.localScale.x, buttonBackground.transform.localScale.y); }
        set { buttonCollider.transform.localScale = buttonBackground.transform.localScale = new Vector3(value.x, value.y, 1.0f); }
    }
    public float TextSize
    {
        get { return buttonText.transform.localScale.x * 100.0f; }
        set { buttonText.transform.localScale = new Vector3(value * 0.01f, value * 0.01f, 1.0f); }
    }
    public Material ButtonMaterial
    {
        get { return buttonBackground.material; }
        set { buttonBackground.material = value; }
    }
    public Color TextColor
    {
        get { return buttonText.color; }
        set { buttonText.color = value; }
    }
}