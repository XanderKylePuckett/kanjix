﻿using UnityEngine;
using KanjixStructs;
public class CharacterDisplay : MonoBehaviour
{
    [SerializeField] private TextMesh characterText = null, furiganaText = null;
    [SerializeField] private MeshRenderer characterBackground = null, furiganaBackground = null;
    public CharacterDisplay SetCharacter(KanjixCharacter character)
    {
        if (character.isKanji)
        {
            characterText.text = character.character.ToString();
            furiganaBackground.enabled = true;
            furiganaBackground.material = characterBackground.material = GameManager.instance.characterBackgrounds[character.level];
            furiganaText.text = character.reading;
        }
        else
        {
            characterText.text = character.character.ToString();
            furiganaBackground.enabled = false;
            characterBackground.material = GameManager.instance.characterBackgrounds[0];
            furiganaText.text = "";
        }
        return this;
    }
}