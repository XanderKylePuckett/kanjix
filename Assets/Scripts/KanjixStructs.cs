﻿namespace KanjixStructs
{
    using System;
    using System.Collections.Generic;
    public enum YearLevel { Year1, Year2, Year1And2, Year3, Year2And3, Year4, Year3And4, Year5, Year4And5, Year6, Year5And6, NumYearLevels, InvalidYearLevel }
    [Serializable]
    public struct KanjixKanji
    {
        public string meaning;
        public int level;
        public char kanji;
    }
    [Serializable]
    public struct KanjixKanjiList
    {
        public KanjixKanji[] list;
    }
    [Serializable]
    public struct KanjixCharacter
    {
        public string reading;
        public int level;
        public char character;
        public bool isKanji;
        public KanjixCharacter(char _character)
        {
            reading = null;
            level = 0;
            character = _character;
            isKanji = false;
        }
    }
    [Serializable]
    public struct KanjixSentence
    {
        public KanjixCharacter[] characters;
        public KanjixSentence(string inSentence)
        {
            List<KanjixCharacter> outSentence = new List<KanjixCharacter>();
            KanjixCharacter tmp;
            for (int i = 0; i < inSentence.Length; ++i)
            {
                tmp = new KanjixCharacter(inSentence[i]);
                if (i + 1 < inSentence.Length && '[' == inSentence[i + 1])
                {
                    tmp.isKanji = true;
                    tmp.reading = "";
                    i += 2;
                    while (i < inSentence.Length)
                    {
                        if (']' != inSentence[i])
                        {
                            tmp.reading += inSentence[i];
                            ++i;
                        }
                        else break;
                    }
                }
                else
                    tmp.reading = tmp.character.ToString();
                outSentence.Add(tmp);
            }
            characters = KanjixLibraries.AssociateKanjiLevels(new KanjixSentence { characters = outSentence.ToArray() }).characters;
        }
        public int Count => null != characters ? characters.Length : 0;
        public KanjixCharacter Get(int character) => (null != characters && character < characters.Length) ? characters[character] : new KanjixCharacter('\0');
    }
    [Serializable]
    public struct KanjixSentencePracticePage
    {
        public KanjixSentence[] sentences;
        public int Count => null != sentences ? sentences.Length : 0;
        public KanjixSentence Get(int sentence) => (null != sentences && sentence < sentences.Length) ? sentences[sentence] : new KanjixSentence { characters = null };
        public KanjixCharacter Get(int sentence, int character) => Get(sentence).Get(character);
    }
    [Serializable]
    public struct KanjixSentencePractice
    {
        public KanjixSentencePracticePage[] pages;
        public int Count => null != pages ? pages.Length : 0;
        public KanjixSentencePracticePage Get(int page) => (null != pages && page < pages.Length) ? pages[page] : new KanjixSentencePracticePage { sentences = null };
        public KanjixSentence Get(int page, int sentence) => Get(page).Get(sentence);
        public KanjixCharacter Get(int page, int sentence, int character) => Get(page).Get(sentence, character);
    }
    [Serializable]
    public struct KanjixSentencePracticeCollection
    {
        public KanjixSentencePractice[] practices;
        public int Count => null != practices ? practices.Length : 0;
        public KanjixSentencePractice Get(int practice) => (null != practices && practice < practices.Length) ? practices[practice] : new KanjixSentencePractice { pages = null };
        public KanjixSentencePracticePage Get(int practice, int page) => Get(practice).Get(page);
        public KanjixSentence Get(int practice, int page, int sentence) => Get(practice).Get(page, sentence);
        public KanjixCharacter Get(int practice, int page, int sentence, int character) => Get(practice).Get(page, sentence, character);
    }
    [Serializable]
    public struct KanjixSentenceLibrary
    {
        public KanjixSentencePracticeCollection[] yearLevels;
        public KanjixSentencePracticeCollection Get(int yearLevel) => (null != yearLevels && yearLevel < yearLevels.Length) ? yearLevels[yearLevel] : new KanjixSentencePracticeCollection { practices = null };
        public KanjixSentencePractice Get(int yearLevel, int practice) => Get(yearLevel).Get(practice);
        public KanjixSentencePracticePage Get(int yearLevel, int practice, int page) => Get(yearLevel).Get(practice, page);
        public KanjixSentence Get(int yearLevel, int practice, int page, int sentence) => Get(yearLevel).Get(practice, page, sentence);
        public KanjixCharacter Get(int yearLevel, int practice, int page, int sentence, int character) => Get(yearLevel).Get(practice, page, sentence, character);
    }
    public class KanjixImportSentence
    {
        public YearLevel year;
        public int practice, page, importNumber;
        public KanjixSentence sentence;
        private static YearLevel GetYearLevel(string _year)
        {
            switch (_year)
            {
                case "1": return YearLevel.Year1;
                case "2": return YearLevel.Year2;
                case "3": return YearLevel.Year3;
                case "4": return YearLevel.Year4;
                case "5": return YearLevel.Year5;
                case "6": return YearLevel.Year6;
                case "1,2": return YearLevel.Year1And2;
                case "2,3": return YearLevel.Year2And3;
                case "3,4": return YearLevel.Year3And4;
                case "4,5": return YearLevel.Year4And5;
                case "5,6": return YearLevel.Year5And6;
                default: return YearLevel.InvalidYearLevel;
            }
        }
        public KanjixImportSentence(string importSentence, int _importNumber = -1)
        {
            string[] importData = importSentence.Split("\t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (4 == importData.Length)
            {
                year = GetYearLevel(importData[0]);
                if (!int.TryParse(importData[1], out practice)) practice = 0;
                if (!int.TryParse(importData[2], out page)) page = 0;
                sentence = new KanjixSentence(importData[3]);
            }
            else
            {
                year = YearLevel.InvalidYearLevel;
                practice = 0;
                page = 0;
                sentence = new KanjixSentence();
            }
            importNumber = _importNumber;
        }
        public bool IsValid => null != sentence.characters && page > 0 && practice > 0 && year < YearLevel.NumYearLevels;
    }
}